#!/usr/bin/env python
"""Small script to make tuning curves."""
import os

import matplotlib.pyplot as plt
import numpy as np
import pyrr

import so3_sampling.so3samp as so3samp


def axang_torotm(axang):
    """Axis-angle to rotation matrix.

    Args:
        axang (type): Description of parameter `axang`.

    Returns:
        type: Description of returned object.

    """
    theta = np.linalg.norm(axang)
    if theta < 1e-6:
        crossp = np.identity(3)
        crossp[0, 1] = axang[2]
        crossp[0, 2] = -axang[1]
        crossp[1, 0] = -axang[2]
        crossp[1, 2] = axang[0]
        crossp[2, 0] = axang[1]
        crossp[2, 1] = -axang[0]
        return crossp
    axis = -axang / theta  # pyrr uses the opposite convention
    mat = pyrr.matrix33.create_from_axis_rotation(axis=axis, theta=theta)
    return mat


# Import various data
data_fldr = './mouse_try/'
spks = np.load(os.path.join(data_fldr, 'spks.npy'))
expinfo = np.load(os.path.join(data_fldr, 'expinfo.npy'),
                  allow_pickle=True).item()
img5 = np.load(os.path.join(data_fldr, 'IMG5_slow.npy'), allow_pickle=True)
F = np.load(os.path.join(data_fldr, 'F.npy'))
Fneu = np.load(os.path.join(data_fldr, 'Fneu.npy'))


# Create a list of the stim files
stimfs = []

for i in range(len(img5)):
    fname = img5[i]['fname']
    stim_file = np.load(os.path.join(data_fldr, fname[0:-5] + '.npy'),
                        allow_pickle=True).item()
    stimfs.append(stim_file)


print('There are', spks.shape[0], 'cells and', spks.shape[1], 'frames')

# Set a specific file as a target
stim_target = 2

neuframes_ind = (expinfo['fneu'][:, 2] == stim_target)

neuframes = expinfo['fneu'][neuframes_ind][:, 0]

ind = np.where(spks[neuframes[0]] > 0)

targs = spks[neuframes[0]][ind]

ax_ang = np.zeros((len(neuframes), 3))

cnt = 0
for i in neuframes:
    neur_frame = expinfo['fneu'][i, 0]
    stim_frame = expinfo['fneu'][i, 1]
    stim_id = expinfo['fneu'][i, 2] - 1
    fname = img5[stim_id]['fname']
    ax_ang[cnt] = stimfs[stim_id]['axis_ang'][stim_frame]
    cnt += 1

rots = so3samp.Rotations()
obj_nrot = ax_ang.shape[0]
obj_rots = np.zeros((obj_nrot, 3, 3))

for i in np.arange(obj_nrot):
    obj_rots[i, ...] = axang_torotm(ax_ang[i])

dist = 1 - (
    obj_rots.reshape((obj_nrot, 9)) @ rots.rots.reshape((rots.nrot, 9)).T / 3
)

neighb = np.argmin(dist, axis=1)


sampl = np.zeros(rots.nrot)
for i in np.arange(obj_nrot):
    sampl[neighb[i]] += 1

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(
    ax_ang[:, 0], ax_ang[:, 1], ax_ang[:, 2],
)

binned = rots.axangles[neighb[:], :]

fig2 = plt.figure()
ax2 = fig2.add_subplot(111, projection='3d')
ax2.scatter(
    binned[:, 0], binned[:, 1], binned[:, 2],
)
plt.show()
