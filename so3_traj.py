#!/usr/bin/env python
"""Plotting the SO(3) trajectory together with the object rotation."""
import sys

import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure
import numpy as np

from matplotlib import animation

from mpl_toolkits.mplot3d.axes3d import get_test_data


movie = np.load('./mouse_try/shepard-stim_axang-118b4.npy', allow_pickle=True).item()

fig = plt.figure(figsize=plt.figaspect(0.5), constrained_layout=True)
ax_3d = fig.add_subplot(1, 2, 1, projection='3d')
ax_img = fig.add_subplot(1, 2, 2)


print(movie['stim'].shape)
print(movie['axis_ang'].shape)
movie_extract = movie['stim'][:500, :, 60:132]
axang = movie['axis_ang'][:500]

def init():
    # ax_img.imshow(movie_extract[0], cmap='gray', vmin=0, vmax=255)
    ax_img.set_axis_off()

    ax_3d.set_axis_off()
    ax_3d.view_init(elev=10, azim=-60)
    ax_3d.set_xlim(-np.pi, np.pi)
    ax_3d.set_ylim(-np.pi, np.pi)
    ax_3d.set_zlim(-np.pi, np.pi)

    ax_3d.plot([-np.pi, np.pi], [0, 0], [0, 0], c='black')
    ax_3d.plot([0, 0], [-np.pi, np.pi], [0, 0], c='black')
    ax_3d.plot([0, 0], [0, 0], [-np.pi, np.pi], c='black')

    ax_3d.text(np.pi, 0, 0, '$\\theta_y$', fontsize=20)
    ax_3d.text(0, np.pi, 0, '$\\theta_z$', fontsize=20)
    ax_3d.text(0, 0, np.pi, '$\\theta_x$', fontsize=20)
    # ax2.set_xlabel('$\\theta_x$')
    # ax2.set_ylabel('$\\theta_y$')
    # ax2.set_zlabel('$\\theta_z$')
    # ax2.set_xticks([.0, .5])
    # ax2.set_yticks([.0, .5])
    # ax2.set_zticks([.0, .5])
    # ax_3d.scatter(axang_rel[:, 0], axang_rel[:, 1], axang_rel[:, 2])
    # for s in c_hull.simplices:
    #     srf = Poly3DCollection(
    #         vert_hull[s],
    #         alpha=.25,
    #         facecolor='#800000',
    #         linewidths=1,
    #         edgecolors='r',
    #     )
    #     ax2.add_collection3d(srf)
    return (fig, )


def animate(i):
    # azimuth angle : 0 deg to 360 deg
    ax_3d.scatter(axang[:2 * i + 1, 1], axang[:2 * i + 1, 2], axang[:2 * i + 1, 0], c='C0')
    ax_img.imshow(movie_extract[2 * i], cmap='gray', vmin=0, vmax=255)
    return (fig, )


# Animate
ani = animation.FuncAnimation(
    fig, animate, init_func=init, frames=200, interval=1, blit=True,
)
fname = 'object_axis_angle'
ani.save(f'{fname}.mp4', writer='ffmpeg', fps=20)
