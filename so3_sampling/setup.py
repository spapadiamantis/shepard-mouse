#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Setup script for the SO(3) sampling package."""

import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='so3samp',
    version='0.0.1',
    author='Hervé Rouault',
    author_email='herve.rouault@univ-amu.fr',
    description='Class containing the SO(3) uniform sampling',
    long_description=long_description,
    long_description_content_type='text/markdown',
    # url='https://github.com/pypa/sampleproject',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    include_package_data=True,
)
