#!/usr/bin/env python
"""Test to visualize a tuning curve."""

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401
from so3samp import Rotations

R = np.array(
    [
        [1.00000000e+00, 1.13231364e-17, -2.86852790e-16],
        [1.13231364e-17, 1.00000000e+00, -1.46845020e-16],
        [-2.86852790e-16, -1.46845020e-16, 1.00000000e+00],
    ],
)


def bumb_func(r, g):
    """Describe bump_func.

    Args:
        r (type): Description of parameter `r`.
        g (type): Description of parameter `g`.

    Returns:
        type: Description of returned object.

    """
    a = 10
    return np.exp(-a * (1 / 3 * np.trace(r.T @ g - 1)))


r = Rotations()

xs = r.axes[:, 0] * np.sin(r.angles)
ys = r.axes[:, 1] * np.sin(r.angles)
zs = r.axes[:, 2] * np.sin(r.angles)
vonmises = np.zeros((r.nrot))


# colormap = plt.colors.ListedColormap(vonmises)

for i in range(r.nrot):
    vonmises[i] = bumb_func(R, r.rots[i])

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

# ax.scatter(xs, ys, zs)
ax.scatter(xs, ys, zs, c=vonmises)
plt.show()
