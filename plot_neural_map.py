import matplotlib.pyplot as plt
import numpy as np

img = np.zeros((1800, 2500))
for cell in range(ICELL):
    img[stat[cell]['ypix'], stat[cell]['xpix']] = 0
    if cell in rank:
        app = np.where(rank == cell)
        if np.shape(app)[-1]>1:
            level = np.max(app)
        else:
            level = 1
        img[stat[cell]['ypix'], stat[cell]['xpix']] = 200 - level
fig, ax = plt.subplots(figsize=(20, 30))
ax.imshow(img, cmap='viridis')
fig.savefig('map.png')
