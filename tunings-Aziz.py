import logging
from PIL import Image
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import pyrr
import so3_sampling.so3samp as so3samp
import statistics as stat
from scipy.optimize import curve_fit
from scipy.special import factorial
from scipy.stats import poisson


def axang_torotm(axang):
    """Axis-angle to rotation matrix

    Parameters
    ----------
    axang : TODO

    Returns
    -------
    TODO

    """
    theta = np.linalg.norm(axang)
    if theta < 1e-6:
        crossp = np.identity(3)
        crossp[0, 1] = axang[2]
        crossp[0, 2] = -axang[1]
        crossp[1, 0] = -axang[2]
        crossp[1, 2] = axang[0]
        crossp[2, 0] = axang[1]
        crossp[2, 1] = -axang[0]
        return crossp
    axis = -axang / theta  # pyrr uses the opposite convention
    mat = pyrr.matrix33.create_from_axis_rotation(axis=axis, theta=theta)
    return mat

def calc_frame_angles():
    angles = []
    angles_frames = []

    for frame_nbr in range(0, 26048):
        stim_frame_in_movie = expinfo['fneu'][frame_nbr][1]
        movie_id = expinfo['fneu'][frame_nbr][2]
        if movie_id != -1:
            angles.append(movies_angles[movie_id - 1][stim_frame_in_movie])
    angles = np.asarray(angles)
    #print("here : ", angles.shape)
# Iterer sur 1-5 pour separer

def get_movie_angles(i):
    temp = np.load(os.path.join(data_fldr, mov_file[i - 1]['fname'][:-4] + 'npy' ), allow_pickle=True).item()
    return temp['axis_ang']

def discretize(angles):
    #print(angles.shape)
    obj_nrot = angles.shape[0]
    obj_rots = np.zeros((obj_nrot, 3, 3))
    rots = RSAMP
    for i in np.arange(obj_nrot):
        obj_rots[i, ...] = axang_torotm(angles[i])
    dist = 1 - obj_rots.reshape((obj_nrot, 9)) @ rots.rots.reshape((rots.nrot,9)).T / 3
    neighb = np.argmin(dist, axis=1)
    binned = rots.axangles[neighb[:],:]
    #print(neighb.shape, binned.shape)
    return neighb

def plotData(binned):
        fig = plt.figure()
        ax = plt.axes(projection='3d')
        data = binned.transpose()
        xdata = data[0]
        ydata = data[1]
        zdata = data[2]
        ax.scatter3D(xdata, ydata, zdata)

def plot_hist(data):
        #vals, visites = np.unique(data, axis=0, return_counts=True)
        fig = plt.figure()
        plt.hist(data, bins = np.arange(-0.5, 10.5, 1.), normalize=True)
        plt.show()

def plot_hist_poisson(no_duplicate_neighb, no_duplicate_neighb_occurences):
        # get poisson deviated random numbers
        data = np.random.poisson(np.mean(no_duplicate_neighb_occurences), no_duplicate_neighb.shape)

        # the bins should be of integer width, because poisson is an integer distribution
        bins = np.arange(11) - 0.5
        entries, bin_edges, patches = plt.hist(data, bins=bins, density=True, label='Data')

        # calculate bin centres
        bin_middles = 0.5 * (bin_edges[1:] + bin_edges[:-1])

        # fit with curve_fit
        parameters, cov_matrix = curve_fit(fit_function, bin_middles, entries)

        # plot poisson-deviation with fitted parameter
        x_plot = np.arange(0, 15)

        plt.plot(
            x_plot,
            fit_function(x_plot, *parameters),
            marker='o', linestyle='',
            label='Fit result',
        )
        plt.legend()
        plt.show()

def remove_duplicates(neighb):
        ind = neighb
        selection = np.ones(len(ind), dtype=bool)
        selection[1:] = ind[1:] != ind[:-1]
        return ind[selection]

def fit_function(k, lamb):
    '''poisson function, parameter lamb is the fit parameter'''
    return poisson.pmf(k, lamb)

def assign_angles():
    angles = []
    frame_ids = []
    for frame_nbr in range(0, 26048):
        stim_frame_in_movie = expinfo['fneu'][frame_nbr][1]
        movie_id = expinfo['fneu'][frame_nbr][2]
        if movie_id != -1:
            angles.append(movies_angles[movie_id - 1][stim_frame_in_movie])
            frame_ids.append(frame_nbr)

    angles = np.asarray(angles)
    frame_ids = np.asarray(frame_ids)
    return discretize(angles), frame_ids

def compute_angle_spikes():
    spks_transpose = spks.transpose()
    res = [0]
    for i in range(1, spks_transpose.shape[0]):
        a = spks_transpose[i]
        average = a[np.nonzero(a)].mean()
        res.append(average)
    return res

def compute_spks_means_per_bin(spks_bins, spks_frame_ids, angles_scores):
    res = np.zeros(max(spks_bins))
    for i in spks_frame_ids:
        res[spks_bins[i]] = res[spks_bins[i]] + angles_scores[i] / 2

class Movie():
    """ Stores the stimuli movies associated data """

    def __init__(self, fname, rsamp):
        """TODO: to be defined1. """
        self.npyfile = np.load(os.path.join(data_fldr, fname + ".npy"), allow_pickle=True).item()
        self.binnedtraj = None

        self.axangs = self.npyfile['axis_ang']
        self.nrot = self.axangs.shape[0]
        self.rots = np.zeros((self.nrot, 3, 3))

        for i in np.arange(self.nrot):
            self.rots[i, ...] = axang_torotm(self.axangs[i])

        rotsflat = rsamp.rots.reshape((rsamp.nrot, 9)).T
        dist = 1 - self.rots.reshape((self.nrot, 9)) @ rotsflat / 3

        self.neighb = np.argmin(dist, axis=1)

# Discretization of SO(3)
RSAMP = so3samp.Rotations()

ICELL = 8637
data_fldr = "./mouse_try/"
spks = np.load(os.path.join(data_fldr, "spks.npy"), allow_pickle=True)
stat = np.load(os.path.join(data_fldr, "stat.npy"), allow_pickle=True)
iscell = (np.load(os.path.join(data_fldr, "iscell.npy"), allow_pickle=True)[:, 0] != 0)
ncell = spks.shape[0]
expinfo = np.load(os.path.join(data_fldr, "expinfo.npy"), allow_pickle=True).item()
movfname = expinfo['movie_file']
logging.info(movfname)
base = os.path.splitext(movfname)[0]
mov_file = np.load(os.path.join(data_fldr, base + ".npy"), allow_pickle=True)
trace = spks[ICELL, :]
movies_angles = []
for i in range(0,5):
    movies_angles.append(get_movie_angles(i))


# movie_angles = get_movie_angles(3)
# neighb, discrete_bins_angles = discretize(movie_angles)
# print(neighb)
# no_duplicate_neighb = remove_duplicates(neighb)
# print(no_duplicate_neighb)
#
# no_duplicate_neighb_occurences = np.bincount(no_duplicate_neighb)
# plot_hist_poisson(no_duplicate_neighb[:72], no_duplicate_neighb_occurences[:72])
# plot_hist_poisson(no_duplicate_neighb[72:], no_duplicate_neighb_occurences[72:])

#spks_bins, spks_frame_ids = assign_angles()
#print(spks_bins.shape)
#print(spks_frame_ids.shape)
#angles_scores = compute_angle_spikes()
#print(angles_scores)
#spks_means = compute_spks_means_per_bin(spks_bins, spks_frame_ids, angles_scores)
#print(spks_means)


for filename in os.listdir(os.getcwd()):
   with open(os.path.join(os.getcwd(), filename), 'r') as f:
      im = Image.open(filename)
      im.save(filename,i,".png", dpi=(951,240))
