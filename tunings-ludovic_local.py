#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Plotting SO(3) tuning curves for the visual cortex."""
import logging
import os
import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pyrr
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401

import so3_sampling.so3samp as so3samp

data_fldr = './mouse_try/'


def axang_torotm(axang):
    """Axis-angle to rotation matrix.

    Args:
        axang (type): Description of parameter `axang`.

    Returns:
        type: Description of returned object.

    """
    theta = np.linalg.norm(axang)
    if theta < 1e-6:
        crossp = np.identity(3)
        crossp[0, 1] = axang[2]
        crossp[0, 2] = -axang[1]
        crossp[1, 0] = -axang[2]
        crossp[1, 2] = axang[0]
        crossp[2, 0] = axang[1]
        crossp[2, 1] = -axang[0]
        return crossp
    axis = -axang / theta  # pyrr uses the opposite convention
    mat = pyrr.matrix33.create_from_axis_rotation(axis=axis, theta=theta)
    return mat


class Movie():
    """Stores the stimuli movies associated data."""

    def __init__(self, fname, rsamp):
        """Short summary.

        Args:
            fname (type): Description of parameter `fname`.
            rsamp (type): Description of parameter `rsamp`.

        """
        self.npyfile = np.load(os.path.join(
            data_fldr, fname + '.npy'), allow_pickle=True).item()
        self.binnedtraj = None

        self.axangs = self.npyfile['axis_ang']
        self.nrot = self.axangs.shape[0]
        self.rots = np.zeros((self.nrot, 3, 3))

        for i in np.arange(self.nrot):
            self.rots[i, ...] = axang_torotm(self.axangs[i])

        rotsflat = rsamp.rots.reshape((rsamp.nrot, 9)).T
        dist = 1 - self.rots.reshape((self.nrot, 9)) @ rotsflat / 3

        self.neighb = np.argmin(dist, axis=1)


def init(data_fldr):
    """Initialize.

    Args:
        data_fldr (type): Description of parameter `data_fldr`.

    Returns:
        type: Description of returned object.

    """
    # Discretization of SO(3)
    rsamp = so3samp.Rotations()
    spks = np.load(os.path.join(data_fldr, 'spks.npy'))
    stat = np.load(os.path.join(data_fldr, 'stat.npy'), allow_pickle=True)
    iscell = (np.load(os.path.join(data_fldr, 'iscell.npy'))[:, 0] != 0)
    ncell = spks.shape[0]
    expinfo = np.load(os.path.join(data_fldr, 'expinfo.npy'),
                      allow_pickle=True).item()
    movfname = expinfo['movie_file']
    logging.info(movfname)
    base = os.path.splitext(movfname)[0]
    mov_file = np.load(os.path.join(
        data_fldr, base + '.npy'), allow_pickle=True)
    return rsamp, spks, stat, iscell, ncell, expinfo, mov_file


def get_stim(stim_id):
    """Short summary.

    Args:
        id of the stimulation movie (int)

    Returns:
        type: np.array with stimuli written in axis angle rep.

    """
    fname = mov_file[stim_id - 1]['fname']
    split_fname = os.path.splitext(fname)[0]
    stim = np.load(os.path.join(
        data_fldr, split_fname + '.npy'), allow_pickle=True,
    ).item()
    axis_ang = stim['axis_ang']

    return axis_ang


# Define colormap
# colors = plt.cm.jet(np.linspace(0,1,10000))
def extract_frames(expinfo, stim_id):
    """Short summary.

    Args:
        expinfo (np.ndarray): Dictionnary with experience parameters.
        stim_id (np.int64): Stimulation movie id.

    Returns:
        type: Description of returned object.

    """
    indx = np.where(expinfo['fneu'][:, -1] == stim_id)[0]
    neural_frames = expinfo['fneu'][indx, 0]
    stim_frames = expinfo['fneu'][indx, 1]

    return neural_frames, stim_frames


def get_data_to_plot(spks, axis_ang, bin_stim, neural_frames, stim_frames):
    """Synchronize data and compute data to plot.

    Args:
        spks (np.array): Description of parameter `spks`.
        axis_ang (np.array): Description of parameter `axis_ang`.
        bin_stim (np.array): Description of parameter `bin_stim`.
        neural_frames (np.array): Description of parameter `neural_frames`.
        stim_frames (np.array): Description of parameter `stim_frames`.

    Returns:
        stim_neural (np.array): Synchronized Stimulation
        with bin is presented at frame N in ax-angl rep.
        bins_tab (np.array): With bin is presented at frame N.
        bin_stim (np.array): Statistic of bin presentation (how many
        time the bin is presented, normalised by nb of entries)

    """
    # Get responses linked to stimulation
    trace = spks[:, neural_frames - 1]
    # Which bin is stim at frame N (ax-ang)
    stim_neural = axis_ang[stim_frames - 1, :]
    # Which bin is stimulated at frame N
    bin_stim = bin_stim[stim_frames - 1]

    # Average response over visits
    bins_tab = np.zeros((len(trace), 648))
    visits = np.ones((len(trace), 648))
    # Go over frames
    print(len(trace[0]))
    for i in range(len(trace[0])):
        bins_tab[:, bin_stim[i]] += trace[:, i]
        visits[:, bin_stim[i]] += 1
    bins_tab = bins_tab / visits

    # Not optimized we are going to replot many time a bin (each time visited)
    return stim_neural, bins_tab, bin_stim


def get_data_to_plot_split(
                        spks, axis_ang, bin_stim, neural_frames, stim_frames):
    """Synchronize data and compute data to plot.

    Split presentation in two.

    Args:
        spks (np.array): Description of parameter `spks`.
        axis_ang (np.array): Description of parameter `axis_ang`.
        bin_stim (np.array): Description of parameter `bin_stim`.
        neural_frames (np.array): Description of parameter `neural_frames`.
        stim_frames (np.array): Description of parameter `stim_frames`.

    Returns:
        stim_neural (np.array): Synchronized Stimulation
        with bin is presented at frame N in ax-angl rep.
        bins_tab (np.array): With bin is presented at frame N.
        bin_stim (np.array): Statistic of bin presentation (how many
        time the bin is presented, normalised by nb of entries)

    """
    # Get responses linked to stimulation
    trace = spks[:, neural_frames - 1]
    # Which bin is stim at frame N (ax-ang)
    stim_neural = axis_ang[stim_frames - 1, :]
    # Which bin is stimulated at frame N
    bin_stim = bin_stim[stim_frames - 1]

    # Average response over visits

    bins_tab_first = np.zeros((len(trace), 648))
    bins_tab_second = np.zeros((len(trace), 648))
    visits_st = np.ones((len(trace), 648))
    visits_snd = np.ones((len(trace), 648))
    count = np.ones(648)
    # Go over frames
    print(len(trace[0]))
    for i in range(len(trace[0])):
        if i < np.int64(len(trace[0]) / 2):
            count[bin_stim[i]] += 1
            visits_st[:, bin_stim[i]] += 1
            bins_tab_first[:, bin_stim[i]] += trace[:, i]
        else:
            visits_snd[:, bin_stim[i]] += 1
            count[bin_stim[i]] += 1
            bins_tab_second[:, bin_stim[i]] += trace[:, i]
    print(count)
    bins_tab_first = bins_tab_first / visits_st
    bins_tab_second = bins_tab_second / visits_snd

    # Not optimized we are going to replot many time a bin (each time visited)
    return stim_neural, bins_tab_first, bins_tab_second, bin_stim


def plot_tuning(ax, ICELL, stim_neural, bins_tab, bin_stim):
    """Add tuning curve for the selected stimulation."""
    ax.scatter(
                stim_neural[:, 0], stim_neural[:, 1], stim_neural[:, 2],
                c=bins_tab[ICELL, bin_stim], norm=matplotlib.colors.LogNorm()
                )


def bin_traj(rots, axis):
    """Bin trajectory into discrete rep of SO3.

    Args:
        rots (type): Description of parameter `rots`.
        axis (type): Description of parameter `axis`.

    Returns:
        type: Description of returned object.

    """
    shape = np.shape(axis)[0]

    mat = np.zeros((shape, 3, 3))
    for k, obj in enumerate(axis):
        mat[k, ...] = axang_torotm(obj)

    # Compute distance between each bin of discrete rep and computed matrices
    dist = 1 - (
        mat.reshape((shape, 9)) @ rots.rots.reshape((rots.nrot, 9)).T / 3
    )
    # Find nearest bin
    neighb = np.argmin(dist, axis=1)

    bins_count = np.zeros(rots.nrot)
    # bins = np.zeros(rots.nrot)
    selection = np.ones(len(neighb), dtype=bool)
    selection[1:] = (neighb[1:] != neighb[:-1])
    neighb_count = neighb[selection]
    for k in neighb_count:
        bins_count[k] += 1

    return bins_count, neighb


def poisson_law(k, n, alpha):
    """Short summary.

    Args:
        k (type): Description of parameter `k`.
        n (type): Description of parameter `N`.
        alpha (type): Description of parameter `alpha`.

    Returns:
        type: Description of returned object.

    """
    p = np.exp(-n * alpha) * (n * alpha) ** k / np.math.factorial(k)
    return p


def plot_bin(rots, axis):
    """Plot binnin stats and 3d Repartition.

    Args:
        rots (type): Description of parameter `rots`.
        axis (type): Description of parameter `axis`.

    Returns:
        type: Description of returned object.

    """
    bins_count, neighb = bin_traj(rots, axis)
    # first = np.histogram(bins_count[:72])
    # end = np.histogram(bins_count[72:])

    vol = rots.compute_volume()
    alpha_st = np.sum(vol[:72]) / 72 / np.sum(vol)
    alpha_end = np.sum(vol[72:]) / 576 / np.sum(vol)
    N = rots.nrot
    indx = np.random.randint(0, np.shape(axis)[0] + 1, 20)
    max_size = np.max(bins_count)
    x = np.int64(np.arange(max_size))
    # mn = np.mean(bins_count)
    # st = np.std(bins_count)
    fig, axs = plt.subplots()
    axs.set_title('Poisson law - 72 first')
    hist = np.histogram(bins_count[:72], np.arange(max_size + 1))[0]
    axs.scatter(np.arange(max_size), hist)

    poisson_lw = np.zeros(len(x))
    for k in x:
        poisson_lw[k] = poisson_law(k, N, alpha_st) * 72

    axs.scatter(x, poisson_lw, color='red')

    for k in x:
        poisson_lw[k] = poisson_law(k, N, alpha_end) * 576

    fig1, axs1 = plt.subplots()
    axs1.set_title('Poisson law - end')
    hist = np.histogram(bins_count[72:], np.arange(max_size + 1))[0]
    axs1.scatter(np.arange(max_size), hist)
    axs1.scatter(x, poisson_lw, color='red')

    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111, projection='3d')
    ax2.scatter(
        axis[indx, 0], axis[indx, 1], axis[indx, 2],
        c=plt.cm.jet(np.linspace(0, 1, len(indx))),
    )
    ax2.scatter(
        rots.axangles[neighb[indx], 0],
        rots.axangles[neighb[indx], 1],
        rots.axangles[neighb[indx], 2],
        c=plt.cm.jet(np.linspace(0, 1, len(indx))), marker='+',
    )

    return bins_count


def plot_axis_angle(axis):
    """From axis_angle film stimuli plot the rep over time.

    Args:
        axis (type): Description of parameter `axis`.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(
        axis[:, 0], axis[:, 1], axis[:, 2],
        c=np.arange(len(axis[:, 0]), norm=matplotlib.colors.LogNorm())
    )
    plt.show()


def norm_factor(scalar_func, vol):
    """Compute normalisation factor.

    Args:
    scalar_func (np.array): tuning curve from neuron
    vol (np.array): volume of discrete cell

    Returns:
    factor (np.float)
    """
    factor = np.sum(scalar_func ** 2 * vol, axis=1)
    return factor


def harmonics(fc, matrix):
    """Compute harmonics."""
    return np.trace(fc @ matrix, axis1=1, axis2=2)


def reproductibility_comp(fourier_st, fourier_snd, rank, id):
    """Compare tunings curves.

    fourier_st : fourier coeff 1st part
    fourier_snd : fourier coeff 2nd part
    rank : cell ranking
    id : film id
    """
    ff = fourier_st.reshape(13019, 9)
    fg = fourier_snd.reshape(13019, 9)
    values = np.zeros(13019)
    ranking = np.flip(rank)
    for i in range(len(ranking)):
        corr = np.corrcoef(ff[ranking[i]], fg[ranking[i]])[0, 1]
        values[i] = corr
    np.save("stim" + str(id) + "corrcoef.npy", values)
    figt, axt = plt.subplots(figsize=(90, 60))
    axt.scatter(np.arange(0, len(values)), values, marker='s')
    axt.set_title('Cross correlation over fourier coefficients')
    axt.set_xlabel('Cell ranking')
    axt.set_ylabel('$C_{01}$')
    axt.axhline(0, xmin=0, xmax=1, linewidth=2, ls='--', color='black')
    figt.savefig('Variation cell ' + ' stim ' + str(id) + '.png')


if __name__ == '__main__':
    RSAMP, spks, stat, iscell, ncell, expinfo, mov_file = init(data_fldr)
    volume = RSAMP.compute_volume()
    ICELL = 8637
    if False:
        fig = plt.figure()
        for id in range(1, 6):
            axis_ang = get_stim(id)
            neural, stim = extract_frames(expinfo, id)
            binned_stim = np.int64(bin_traj(RSAMP, axis_ang)[-1])
            binned_stim_axang = RSAMP.axangles[binned_stim]
            stim_neural, bins_tab, bin_stim = get_data_to_plot(
                                spks, binned_stim_axang, binned_stim, neural,
                                stim)
            ax = fig.add_subplot(2, 3, id, projection='3d')
            plot_tuning(ax, ICELL, stim_neural, bins_tab, bin_stim)
            print(str(id) + ' done')

    if True:
        spectrum = np.zeros((5, 13019))
        fourier = np.zeros((5, 13019, 3, 3))
        rank = np.zeros((5, 13019))
        harm = np.zeros((5, 648))
        fig = plt.figure()
        fig2 = plt.figure()
        for id in range(1, 6):
            axis_ang = get_stim(id)
            neural, stim = extract_frames(expinfo, id)
            _, binned_stim = bin_traj(RSAMP, axis_ang)
            binned_stim_axang = RSAMP.axangles[binned_stim]
            stim_neural, bins_tab, bin_stim = get_data_to_plot(
                                spks, binned_stim_axang, binned_stim, neural,
                                stim)
            fourier[id - 1] = RSAMP.fourier_1(bins_tab)
            norm = norm_factor(bins_tab, volume)
            spectrum[id - 1] = np.sum(np.sum(
                                            fourier[id - 1] ** 2, axis=1
                                            ), axis=1) / norm

            rank[id - 1] = np.argsort(spectrum[id - 1])

            ICELL = np.int64(rank[id - 1, -1])
            ax = fig.add_subplot(2, 3, id, projection='3d')
            ax.set_title('Cell ' + str(ICELL) + ' stim ' + str(id))
            plot_tuning(ax, ICELL, stim_neural, bins_tab, bin_stim)

            harm[id - 1] = harmonics(fourier[id - 1, ICELL], RSAMP.rots)
            ax2 = fig2.add_subplot(2, 3, id, projection='3d')
            ax2.set_title('Cell ' + str(ICELL) + ' stim ' + str(id))
            ax2.scatter(RSAMP.axangles[:, 0],
                        RSAMP.axangles[:, 1],
                        RSAMP.axangles[:, 2], c=harm[id - 1])
            print(str(id) + ' done')
        np.save('ranking.npy', rank)
        np.save('spectrum_raw.npy', spectrum)
        np.save('fourier_coeff.npy', fourier)

        print('Plotting ...')

    if True:
        spectrum = np.zeros((5, 2, 13019))
        fourier = np.zeros((5, 2, 13019, 3, 3))
        harm = np.zeros((5, 2, 648))

        for id in range(1, 6):
            axis_ang = get_stim(id)
            neural, stim = extract_frames(expinfo, id)
            _, binned_stim = bin_traj(RSAMP, axis_ang)
            binned_stim_axang = RSAMP.axangles[binned_stim]
            stim_neural, bins_tab_st, bins_tab_snd, bin_stim = \
                get_data_to_plot_split(
                                spks, binned_stim_axang, binned_stim, neural,
                                stim)
            fourier[id - 1, 0] = RSAMP.fourier_1(bins_tab_st)
            fourier[id - 1, 1] = RSAMP.fourier_1(bins_tab_snd)

            norm_st = norm_factor(bins_tab_st, volume)
            norm_snd = norm_factor(bins_tab_snd, volume)

            spectrum[id - 1, 0] = np.sum(np.sum(
                                                fourier[id - 1, 0] ** 2, axis=1
                                                ), axis=1) / norm_st
            spectrum[id - 1, 1] = np.sum(np.sum(
                                                fourier[id - 1, 1] ** 2, axis=1
                                                ), axis=1) / norm_snd

            ICELL = np.int64(rank[id - 1, -1])

            reproductibility_comp(fourier[id - 1, 0], fourier[id - 1, 1], np.int64(rank[id - 1]), id)

            figure = plt.figure(figsize=(20, 20))
            ax = figure.add_subplot(1, 2, 1, projection='3d')
            ax.set_title('Cell ' + str(ICELL) + ' stim ' + str(id) + ' 3st')
            plot_tuning(ax, ICELL, stim_neural, bins_tab_st + 1, bin_stim)

            ax4 = figure.add_subplot(1, 2, 2, projection='3d')
            ax4.set_title('Cell ' + str(ICELL) + ' stim ' + str(id))
            plot_tuning(ax4, ICELL, stim_neural, bins_tab_snd + 1, bin_stim)
            figure.savefig('Cell ' + str(ICELL) + ' stim ' + str(id) + '.png')

            harm[id - 1, 0] = harmonics(fourier[id - 1, 0, ICELL], RSAMP.rots)
            harm[id - 1, 1] = harmonics(fourier[id - 1, 1, ICELL], RSAMP.rots)
            fig2 = plt.figure(figsize=(20, 20))
            ax2 = fig2.add_subplot(1, 2, 1, projection='3d')
            ax2.set_title('First part')
            ax2.scatter(RSAMP.axangles[:, 0],
                        RSAMP.axangles[:, 1],
                        RSAMP.axangles[:, 2], c=harm[id - 1, 0])

            ax3 = fig2.add_subplot(1, 2, 2, projection='3d')
            ax3.set_title('Second part')
            ax3.scatter(RSAMP.axangles[:, 0],
                        RSAMP.axangles[:, 1],
                        RSAMP.axangles[:, 2], c=harm[id - 1, 1])
            fig2.savefig(
                'Harmonicscell' + str(ICELL) + 'stim' + str(id) + '.svg'
                        )
            print(str(id) + ' done')

        print('Trace processing finished')

        img = np.zeros((1800, 2500))
        for cell in range(ICELL):
            img[stat[cell]['ypix'], stat[cell]['xpix']] = -1

            # if cell in rank:
            #    app = np.where(rank == cell)
            #    if np.shape(app)[-1] > 1:
            #        level = np.max(app)
            #    else:
            #        level = 1
            #    img[stat[cell]['ypix'], stat[cell]['xpix']] = 101 - level
            if cell in rank[:, -100:]:
                img[stat[cell]['ypix'], stat[cell]['xpix']] = 1

        # cmap = plt.cm.jet
        # cmaplist = [cmap(i) for i in range(cmap.N)]
        # cmaplist[0] = (0, 0, 0, 0)
        # cmaplist[1] = (1.0, 1.0, 1.0, 1.0)
        # cmap = matplotlib.colors.LinearSegmentedColormap.from_list('Custom', cmaplist, cmap.N)
        # bounds = np.arange(0, 102)
        # norm = matplotlib.colors.BoundaryNorm(bounds, cmap.N)

        fig, ax = plt.subplots(figsize=(20, 30))
        ax.imshow(img, cmap='bwr')
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        # ax.imshow(img, cmap=cmap, norm=norm)
        fig.savefig('map_flat.svg')

    sys.exit()

# if __name__ == "__main__":
#     # execute only if run as a script
#     main()
