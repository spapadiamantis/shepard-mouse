"""Small script to make tuning curves"""
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pyrr

import so3_sampling.so3samp as so3samp


def axang_torotm(axang):
    """Axis-angle to rotation matrix

    Parameters
    ----------
    axang : TODO

    Returns
    -------
    TODO

    """
    theta = np.linalg.norm(axang)
    if theta < 1e-6:
        crossp = np.identity(3)
        crossp[0, 1] = axang[2]
        crossp[0, 2] = -axang[1]
        crossp[1, 0] = -axang[2]
        crossp[1, 2] = axang[0]
        crossp[2, 0] = axang[1]
        crossp[2, 1] = -axang[0]
        return crossp
    axis = -axang / theta  # pyrr uses the opposite convention
    mat = pyrr.matrix33.create_from_axis_rotation(axis=axis, theta=theta)
    return mat


# Set data folder string variable
data_fldr = './mouse_try/'

# Import data
spks = np.load(os.path.join(data_fldr, 'spks.npy'))
expinfo = np.load(os.path.join(data_fldr, 'expinfo.npy'),
                  allow_pickle=True).item()
img5 = np.load(os.path.join(data_fldr, 'IMG5_slow.npy'), allow_pickle=True)
F = np.load(os.path.join(data_fldr, 'F.npy'))
Fneu = np.load(os.path.join(data_fldr, 'Fneu.npy'))

# Create Rotations class object
rots = so3samp.Rotations()

# Create a list of the stim files
stimfs = []

for i in range(len(img5)):

    fname = img5[i]['fname']
    stim_file = np.load(os.path.join(data_fldr, fname[0:-5] + ".npy"),
                        allow_pickle=True).item()
    stimfs.append(stim_file)


def remove_concecutives(ind):
    """
    Simple function that removes concecutive
    duplicate values from and array of indices

    Parameters
    ----------
    ind : numpy array of indices

    Returns
    -------
    ind[selection]: numpy array without concecutive duplicates
    """

    # Remove concevutive entries in array
    selection = np.ones(len(ind), dtype=bool)
    selection[1:] = ind[1:] != ind[:-1]
    return ind[selection]


def get_indices_for_stim(stim_id):
    """
    Get all indices of the array that
    feature the object with stim_id

    Parameters
    ----------
    stim_id : the id of the Shepard Metzler object

    Returns
    -------
    array of indices with id == stim id
    """

    return expinfo['fneu'][:, 2] == stim_id


def get_frames(ind):
    """
    Get ids of all neural frames that
    appear in an array of indices

    Parameters
    ----------
    ind : array of indices

    Returns
    -------
    array of neural fram ids based on ind
    """

    return expinfo['fneu'][:, 1][ind]


def ax_ang_bins_to_ind(binned, ax_angles):
    """
    Auxiliary function to get indices of
    binned axis angles values

    Parameters
    ----------
    binned : array of binned axis angles
    ax_angles : axis angle array of Rotations class object

    Returns
    ------
    ind : array of indices
    """

    ind = np.zeros(len(binned))

    for i in range(len(binned)):
        for j in range(len(ax_angles)):
            if np.allclose(ax_angles[j], binned[i]):
                ind[i] = j

    return ind


def get_neighbourhood(stim_id, indices):
    """
    Get array of indices of all rotations in
    respect to Rotations class bin with stim_id

    Parameters
    ----------
    stim_id : Shepard Metzler object id
    indices : indices of neural frames to search

    Returns
    ------
    binned : axis angles representations binned to class bins
    neigh : indices of elements in the Rotations.ax_angle structure
    """

    ax_ang = stimfs[stim_id]['axis_ang'][indices]
    obj_nrot = ax_ang.shape[0]
    obj_rots = np.zeros((obj_nrot, 3, 3))

    for i in np.arange(obj_nrot):
        obj_rots[i, ...] = axang_torotm(ax_ang[i])

    dist = 1 - obj_rots.reshape((obj_nrot, 9)) @ rots.rots.reshape((rots.nrot,
                                                                    9)).T / 3

    neighb = np.argmin(dist, axis=1)

    binned = rots.axangles[neighb[:], :]

    return binned, neighb


def get_tuning_curve(stim_id):
    """
    Calculate cell tuning curves for objec with
    stim_id

    Parameters
    ----------
    stim_id : Shepard Metzler object id

    Returns
    ------
    res : array of spikes normalized by number of visits
    n_visits : number of visits to each bin
    """

    ind = get_indices_for_stim(stim_id)
    frames = get_frames(ind)
    binned, neighb = get_neighbourhood(stim_id, frames)
    # neighb = remove_concecutives(neighb)
    spikes = np.zeros((spks.shape[0], len(rots.axangles)))
    n_visits = np.zeros(len(rots.axangles))

    print(neighb.shape)
    print(spks.shape)
    for i in np.arange(neighb.shape[0]):
        spikes[:, neighb[i]] += spks[:, i]
        n_visits[neighb[i]] += 1

    res = spikes / (n_visits + 1)
    return res, n_visits


def plot_tuning_curve(t_c):
    """
    Plot 3D representation of tuning curve
    on rots.axangles representation

    Parameters:
    t_c : a single tuning curve

    Returns:
    -
    """
    xs = rots.axangles[:, 0]
    ys = rots.axangles[:, 1]
    zs = rots.axangles[:, 2]
    fig = plt.figure()

    ax = fig.add_subplot(111, projection='3d')
    h = ax.scatter(xs, ys, zs, c=t_c)
    fig.colorbar(h)
    plt.show()


def get_all_tuning_curves(stim_id):

    # for each image find at which bin it corresponds
    # increment by spike the table of cell
    n_cells = spks.shape[0]
    n_bins = len(rots.axangles)
    t_curves = np.zeros((n_cells, n_bins))
    for i in range(n_cells):
        sys.stdout.write("\r{:e.2f}".format((float(i) / n_cells) * 100))
        sys.stdout.flush()
    #    for j in range(n_objects):
        t_curves[i], _ = get_tuning_curve(stim_id)

    np.save('tuning_curves.npy', t_curves)

    return t_curves


def load_tuning_curves():
    """
    Auxiliary function to load tuning curves from
    file if needed

    Parameters
    ----------
    -

    Returns
    ------
    t_c : tuning curve array
    """

    t_c = np.load('tuning_curves.npy')
    return t_c


def parseval_identity(fourier, data):
    """
    Calculate Parseval's identity

    Parameters
    ----------
    fourier: 3x3 matrix of discrete fourier transform
    data : array of tuning curves

    Returns
    ------
    : parseval's identity value to see which cell
    produces the most energy
    """
    N = len(data)
    parseval_2 = np.zeros(N)
    parseval_1 = np.sum((data ** 2) * rots.volumes, axis=1)
    parseval_2 = (2 * np.sum(fourier**2, axis=(1, 2))) / N
    return parseval_2 / parseval_1


def fourier_harmonics(fourier):
    """
    Calculate harmonics of Fourier trasform for
    a single cell and plot it

    Parameters
    ----------
    fourier: 3x3 matrix of discrete fourier transform

    Returns
    ------
    harmonics: array of harmonics of the transform
    """

    harmonics = fourier @ np.transpose(rots.rots, (0, 2, 1))
    harmonics = np.trace(harmonics, axis1=1, axis2=2)

    plot_tuning_curve(harmonics)

    return harmonics


def reproducibilty_test(rank):

    # Create boolean indices test for devision
    global spks

    # Split arrays
    spks1 = spks[:,0:int(spks.shape[1]/2)]
    spks2 = spks[:,int(spks.shape[1]/2):]

    print(spks1.shape)
    print(spks2.shape)
    # Calculate tuning curves and Fourier for first array
    spks = spks1
    t_curves1,_ = get_tuning_curve(3)

    # Calculate tuning curves and Fourier for first array
    spks = spks2
    t_curves2, _ = get_tuning_curve(3)

    values = np.zeros(len(t_curves1))
    print(values.shape)
    for i in range(len(rank)):
        values[i] = np.corrcoef(t_curves1[rank[i]], t_curves2[rank[i]])[0, 1]

    fig = plt.figure()
    plt.scatter(np.arange(0, len(values)), values, marker='s')
    plt.show()

    print(np.mean(values))
    print(np.sum(np.mean(t_curves1,axis=1)/np.mean(t_curves2,axis=1)))

def spike_plot():

    colors = plt.rcParams["axes.prop_cycle"]()
    global spks
    x = np.arange(0,2000,1)
    fig, axs = plt.subplots(5)
    fig.suptitle("Deconvolved signal",fontsize=20)
    for i in range(5):
        c = next(colors)["color"]
        axs[i].plot(x, spks[i][0:2000],color=c)
        if i != 4:
            axs[i].set_axis_off()

    plt.xlabel("Time")
    plt.ylabel("Amplitude")
    plt.show()
