import numpy as np
import pyrr
import so3samp
import matplotlib.pyplot as plt


def axang_torotm(axang):
    """Axis-angle to rotation matrix

    Parameters
    ----------
    axang : TODO

    Returns
    -------
    TODO

    """
    theta = np.linalg.norm(axang)
    if theta < 1e-6:
        crossp = np.identity(3)
        crossp[0, 1] = axang[2]
        crossp[0, 2] = -axang[1]
        crossp[1, 0] = -axang[2]
        crossp[1, 2] = axang[0]
        crossp[2, 0] = axang[1]
        crossp[2, 1] = -axang[0]
        return crossp
    axis = -axang / theta  # pyrr uses the opposite convention
    mat = pyrr.matrix33.create_from_axis_rotation(axis=axis, theta=theta)
    return mat

dir="mouse_try/"
spks=np.load(dir+"spks.npy",allow_pickle=True)
expinfo=np.load(dir+"expinfo.npy",allow_pickle=True).item()
fneu=expinfo['fneu']
frames=fneu[:,0]
stim_id=fneu[:,2]
IMG5_slow=np.load(dir+"IMG5_slow.npy",allow_pickle=True)
tab=np.zeros((len(frames),5))
obj_rots = np.zeros((len(frames), 3, 3))
Films=[]
#Recuperation des données
for i in range(5):
	fname=IMG5_slow[i]['fname']
	fname=fname.split(".")[0]+".npy"
	film=np.load(dir+fname,allow_pickle=True)
	Films.append(film)
	
for i in frames:
	id=stim_id[i-1]
	if id==-1:
		continue

	film=Films[id-1]
	aa=film.item()["axis_ang"][int(((i-1)%766)*10000/725)-1,:]
	obj_rots[i-1,:,:]=axang_torotm(aa)
	tab[i-1,:]=np.array([i,aa[0],aa[1],aa[2],id])

tabc=np.hstack((tab,spks.T))
obj_rots2=obj_rots[tabc[:,4]!=0,:,:]	#Sans les moments d'interruption entre les films
tab2=tabc[tabc[:,4]!=0]
tab2=tab2[:-3,:]
obj_rots2=obj_rots2[:-3,:,:]

rots = so3samp.Rotations()

obj_rots_flu=obj_rots2[:3625,:,:]	#Les 5 premiers films (tous différents)
tab_flu=tab2[:3625,:]

dist_flu = 1 - obj_rots_flu.reshape((len(tab_flu), 9)) @ rots.rots.reshape((rots.nrot,9)).T / 3
voisin_flu=np.argmin(dist_flu,axis=1)
visites_flu=np.zeros(rots.nrot)
for i in range(rots.nrot):
	visites_flu[i]=np.sum(voisin_flu==i)

def hist_poisson(m,visites):
	bins=np.arange(int(np.max(visites)))
	counts,bin_edges=np.histogram(visites,bins=bins-0.5)
	d=bin_edges[1]-bin_edges[0]
	plt.hist(visites,bins=bins-0.5)
	plt.errorbar(bin_edges[:-1]+d/2,counts,np.sqrt(counts),fmt="none")
	plt.plot(bins[:-1],np.sum(counts)*poisson.pmf(bins[:-1],m),".")
	
m1=np.mean(visites_flu[:73])
v1=np.std(visites_flu[:73])**2
m=np.mean(visites_flu[73:])
v=np.std(visites_flu[73:])**2
plt.subplot(121)
hist_poisson(m1,visites_flu[:73])
plt.subplot(122)
hist_poisson(m,visites_flu[73:])
plt.show()

def tuning(calc,indices,cells):
	c=np.zeros((648,cells))
	for p in range(648):
		if np.sum(indices==p)==0:
			c[p]=0
		else:
			c[p]=np.sum(calc[indices==p],axis=0)/np.sum(indices==p)
	return c
I=4 #Numéro du film
cell=0	#Numéro de la cellule
pres=4	#Numéro de la présentation du film
tabI=tab2[tab2[:,4]==I]
obj_rotsI=obj_rots2[tab2[:,4]==I,:,:]
distI = 1 - obj_rotsI.reshape((len(tabI), 9)) @ rots.rots.reshape((rots.nrot,9)).T / 3
voisinI=np.argmin(distI,axis=1)
indices=voisinI.reshape(725,-1)
pos=rots.axangles
calc=tabI[:,cell+5].reshape(725,-1)
c=tuning(calc[:,pres],indices[:,pres],1)

fig = plt.figure()
ax = plt.axes(projection ='3d')
ax.scatter3D(pos[:,0], pos[:,1], pos[:,2],c=c)
plt.show()

volumes=rots.volumes
dist=1 - obj_rots2.reshape((len(tab2), 9)) @ rots.rots.reshape((rots.nrot,9)).T / 3
voisin=np.argmin(dist,axis=1)
pos=rots.axangles

rapport=np.zeros((34,13019))
for i in range(34):
	calci=tab2[725*i:725*(i+1),5:]
	obj_rotsi=obj_rots2[725*i:725*(i+1),:,:]
	dist=1-obj_rotsi.reshape(725,9)
	voisin=np.argmin(dist,axis=1)
	t=tuning(calci,voisin,13019)
	mat=rots.fourier_1(t.T)
	rapport[i]=np.sum(mat**2,axis=(1,2))/np.sum(t*volumes[:,None],axis=0)

maxcells=np.argsort(rapport,axis=1)[:,-11:]
film1=[4,6,13,20,23,30,31]
film2=[5,7,14,18,25,27,32]
film3=[1,8,12,19,21,26]
film4=[3,9,15,16,22,28,33]
film5=[2,10,11,17,24,29,34]
for i in film4:
	plt.plot(maxcells[i-1],".")
plt.show()